mysql -u root -p

USE music_store;

-- 1.
SELECT * FROM artists WHERE name LIKE "%D%";

-- 2.
SELECT * FROM songs WHERE length < 230;

-- 3.
SELECT album_title, song_name, length FROM albums
JOIN songs ON songs.album_id = albums.id
JOIN artists ON artists.id = albums.artist_id;

-- 4.
SELECT * FROM albums
JOIN artists ON artists.id = albums.artist_id AND albums.album_title LIKE "%A%";

--5. 
SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;

-- 6.
SELECT * FROM albums
JOIN songs ON songs.album_id = albums.id 
ORDER BY albums.album_title DESC, songs.song_name ASC;

-- Author: Harvy G. Logroño